# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'askdim.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog
import os


class Ui_PCB_dim(object):
    def setupUi(self, PCB_dim):
        PCB_dim.setObjectName("PCB_dim")
        PCB_dim.resize(353, 314)
        PCB_dim.setAutoFillBackground(False)
        self.verticalLayoutWidget = QtWidgets.QWidget(PCB_dim)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(0, 0, 351, 311))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.pcb_width = QtWidgets.QDoubleSpinBox(self.verticalLayoutWidget)
        self.pcb_width.setMinimum(10.0)
        self.pcb_width.setMaximum(10000.0)
        self.pcb_width.setProperty("value", 50.0)
        self.pcb_width.setObjectName("pcb_width")
        self.horizontalLayout.addWidget(self.pcb_width)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_2 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_2.addWidget(self.label_2)
        self.pcb_height = QtWidgets.QDoubleSpinBox(self.verticalLayoutWidget)
        self.pcb_height.setMinimum(10.0)
        self.pcb_height.setMaximum(10000.0)
        self.pcb_height.setProperty("value", 50.0)
        self.pcb_height.setObjectName("pcb_height")
        self.horizontalLayout_2.addWidget(self.pcb_height)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_3 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_4.addWidget(self.label_3)
        self.pcb_metric = QtWidgets.QDoubleSpinBox(self.verticalLayoutWidget)
        self.pcb_metric.setMinimum(1.0)
        self.pcb_metric.setMaximum(100.0)
        self.pcb_metric.setProperty("value", 2.0)
        self.pcb_metric.setObjectName("pcb_metric")
        self.horizontalLayout_4.addWidget(self.pcb_metric)
        self.verticalLayout_2.addLayout(self.horizontalLayout_4)
        self.verticalLayout.addLayout(self.verticalLayout_2)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.label_4 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_5.addWidget(self.label_4)
        self.pcb_corner = QtWidgets.QDoubleSpinBox(self.verticalLayoutWidget)
        self.pcb_corner.setMaximum(100000.0)
        self.pcb_corner.setProperty("value", 1.0)
        self.pcb_corner.setObjectName("pcb_corner")
        self.horizontalLayout_5.addWidget(self.pcb_corner)
        self.verticalLayout_3.addLayout(self.horizontalLayout_5)
        self.verticalLayout.addLayout(self.verticalLayout_3)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_5 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_5.setAlignment(QtCore.Qt.AlignCenter)
        self.label_5.setObjectName("label_5")
        self.horizontalLayout_3.addWidget(self.label_5)
        self.path = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.path.setObjectName("path")
        self.horizontalLayout_3.addWidget(self.path)
        self.Browse = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.Browse.setObjectName("Browse")
        self.horizontalLayout_3.addWidget(self.Browse)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.folder_struct_button = QtWidgets.QCheckBox(self.verticalLayoutWidget)
        self.folder_struct_button.setObjectName("folder_struct_button")
        self.verticalLayout.addWidget(self.folder_struct_button)
        self.buttonBox = QtWidgets.QDialogButtonBox(self.verticalLayoutWidget)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(PCB_dim)
        self.buttonBox.accepted.connect(PCB_dim.accept)
        self.buttonBox.rejected.connect(PCB_dim.reject)
        QtCore.QMetaObject.connectSlotsByName(PCB_dim)

    def retranslateUi(self, PCB_dim):
        _translate = QtCore.QCoreApplication.translate
        PCB_dim.setWindowTitle(_translate("PCB_dim", "Enter PCB dimensions"))
        self.label.setText(_translate("PCB_dim", "Width (mm)"))
        self.label_2.setText(_translate("PCB_dim", "Height (mm)"))
        self.label_3.setText(_translate("PCB_dim", "Metric hole (mm)"))
        self.label_4.setText(_translate("PCB_dim", "Corner radius (mm)"))
        self.label_5.setText(_translate("PCB_dim", "Path"))
        self.Browse.setText(_translate("PCB_dim", "Browse"))
        self.folder_struct_button.setText(_translate("PCB_dim", "Create folder structure"))
        self.Browse.clicked.connect(self.browse)

    def browse(self):
        self.path.setText(
            str(QFileDialog.getExistingDirectory(caption="Select Directory", directory=os.path.expanduser("~"))))



if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    PCB_dim = QtWidgets.QDialog()
    ui = Ui_PCB_dim()
    ui.setupUi(PCB_dim)
    PCB_dim.show()
    sys.exit(app.exec_())
