# KiCAD Initializer

This script creates an edge with the desired dimensions and folder structure in
the provided KiCAD project path. 

Just need to define the folders in the structure.txt. 

![](media/gif.mov)


## Install

clone the repo anywhere

## Run


+ Edit the project path variable

+ The pcbnew file must have been opened at least once

+ Run the script:


`./kicad_init.py`

or 

` python3 kicad_init.py `

## Future developments

* Turn it into a PCBNEW plugin
* Merge folder browsing with pcb specification
* Add default silkscreen logo 

