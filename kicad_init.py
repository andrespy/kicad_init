#!/usr/bin/env python3

import os
import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QFileDialog
import create_edge_cuts



#app = QApplication(sys.argv)
#win = QMainWindow()
#win.setGeometry(200,200,300,300) # sets the windows x, y, width, height
#win.setWindowTitle("Project directory") # setting the window title
#project_path = str(QFileDialog.getExistingDirectory(caption="Select Directory",directory=os.path.expanduser("~")))
#del win , app

#project_name = project_path.split(sep='/')[-1]

sf = "structure.txt"
create_structure, project_path = create_edge_cuts.format_file()
if create_structure:
    try:
        f = open(sf)
    except EnvironmentError as e:
        print(e)
        print("Using default settings")
        lines = ['3d_models', 'datasheets', 'gerber', 'images', 'lib_sch', 'lib_footprint.pretty', 'pdf']
    else:
        lines = f.read().splitlines()
        f.close()

    if not os.path.exists(project_path):
        print(project_path + "not found. Create a blank project with KiCAD")
        exit(1)

    for line in lines:
        try:
            os.mkdir(project_path + "/" + line)
        except EnvironmentError as e:
            print(e)
        else:
            print(project_path + "/" + line + " created")

exit(1)





