import sys
from askdim import *


def edge_line(x0, y0, x1, y1):
    return "(gr_line (start " + \
           str(x0) + " " + str(y0) + ") (end " + str(x1) + " " + str(y1) + ") (layer Edge.Cuts) (width 0.05))\n"


# (gr_line (start 114 48) (end 135 48) (layer Edge.Cuts) (width 0.05))

def edge_arc(x0, y0, x1, y1, angle):
    return "(gr_arc (start " + \
           str(x0) + " " + str(y0) + ") (end " + str(x1) + " " + str(y1) + ") (angle " + str(
        angle) + ") (layer Edge.Cuts) (width 0.05))\n"


# (gr_arc (start 195 65) (end 219 65) (angle -90) (layer F.SilkS) (width 0.12))

def edge_circle(x0, y0, x1, y1):
    return "(gr_circle (center " + \
           str(x0) + " " + str(y0) + ") (end " + str(x1) + " " + str(y1) + ") (layer Edge.Cuts) (width 0.05))\n"


def edge_end():
    return ")"


def format_file():
    app = QtWidgets.QApplication(sys.argv)
    PCB_dim = QtWidgets.QDialog()
    ui = Ui_PCB_dim()
    ui.setupUi(PCB_dim)
    PCB_dim.show()
    app.exec_()
    project = ui.path.text()
    pcb_path = project + '/' + project.split(sep='/')[-1] + ".kicad_pcb"
    w = ui.pcb_width.value()
    h = ui.pcb_height.value()
    r = ui.pcb_corner.value()
    m = ui.pcb_metric.value() / 2
    create_struct = ui.folder_struct_button.checkState()

    try:
        pcb = open(pcb_path, 'r')
    except EnvironmentError as e:
        print(e)
    else:
        # w = int(input("Enter PCB width (mm): "))
        # h = int(input("Enter PCB height (mm): "))
        # r = int(input("Enter PCB corner radius (mm): "))
        # m = int(input("Enter PCB mounting hole radius (mm): "))
        mid_x = 150 - w / 2
        mid_y = 100 - h / 2
        lines = pcb.readlines()
        lines = lines[:-1]
        pcb.close()

        pcb = open(pcb_path, 'w')
        lines.append(edge_line(mid_x + r, mid_y, mid_x + w - r, mid_y))
        lines.append(edge_line(mid_x + r, mid_y + h, mid_x + w - r, mid_y + h))
        lines.append(edge_line(mid_x, mid_y + r, mid_x, mid_y + h - r))
        lines.append(edge_line(mid_x + w, mid_y + r, mid_x + w, mid_y + h - r))

        arcx_0 = mid_x + r
        arcx_1 = mid_x + w - r
        arcy_0 = mid_y + r
        arcy_1 = mid_y + h - r

        lines.append(edge_arc(arcx_0, arcy_0, arcx_0, arcy_0 - r, -90))
        lines.append(edge_arc(arcx_0, arcy_1, arcx_0 - r, arcy_1, -90))
        lines.append(edge_arc(arcx_1, arcy_0, arcx_1 + r, arcy_0, -90))
        lines.append(edge_arc(arcx_1, arcy_1, arcx_1, arcy_1 + r, -90))

        circx_0 = mid_x + m + 1
        circx_1 = mid_x + w - m - 1
        circy_0 = mid_y + m + 1
        circy_1 = mid_y + h - m - 1

        lines.append(edge_circle(circx_0, circy_0, circx_0, circy_0 - m))
        lines.append(edge_circle(circx_0, circy_1, circx_0 - m, circy_1))
        lines.append(edge_circle(circx_1, circy_0, circx_1 + m, circy_0))
        lines.append(edge_circle(circx_1, circy_1, circx_1, circy_1 + m))

        lines.append(edge_end())

        pcb.writelines(lines)
        pcb.close()

    return create_struct, project
